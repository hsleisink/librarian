var newdir_dialog, newtxtfile_dialog, rename_dialog, text_changed;

function toggle_trunk(arrow) {
	var src = $(arrow).attr('src');
	$(arrow).attr('src', src.substring(0, 23) + (1 - parseInt(src.substring(23, 24))) + src.substring(24));

	var trunk = $(arrow).parent().parent().next();
	if ($(trunk).hasClass('trunk')) {
		$(trunk).toggle();
	}
}

function click_anchor(obj) {
	document.location = $(obj).find('a').attr('href');
}

function dd(s) {
	if (s < 10) {
		s = '0' + s;
	}

	return s;
}

function now() {
	var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	var d = new Date();

	var date = [ d.getDate(), months[d.getMonth()], d.getFullYear() ];
	var time = [ dd(d.getHours()), dd(d.getMinutes()), dd(d.getSeconds()) ];

	return date.join(' ') + ', ' + time.join(':');
}

function error_message(message) {
	var html = $('<div title="Error"><p>' + message + '</div>');
	html.dialog();
}

function add_total(value) {
	var total = $('table.list tfoot td:nth-child(3)').text().split(': ');
	$('table.list tfoot td:nth-child(3)').text(total[0] + ': ' + (parseInt(total[1]) + value));
}

function make_draggable(item) {
	item.draggable({
		helper: 'clone',
		revert: true,
		revertDuration: 100,
		drag: function(event, ui) {
			ui.helper.css('background-image', $(this).parent().css('background-image'));
		},
		cursorAt: { right: 15, bottom: 5 }
	});
}

function new_directory() {
	$('input#newdir').val("");
	newdir_dialog.open();
	$('input#newdir').focus();
}

function new_text_file() {
	$('input#newtxtfile').val(".txt");
	$('input#newtxtfile')[0].setSelectionRange(0, 0);
	newtxtfile_dialog.open();
	$('input#newtxtfile').focus();
}

function rename_file(tr) {
	var filename_current = tr.find('td:first-child').text();
	$('input#renamefile_current').val(filename_current);
	$('input#renamefile_new').val(filename_current);
	rename_dialog.open();
	$('input#renamefile_new').focus();
}

function delete_file(filename) {
	if (confirm('Delete ' + filename + '?')) {
		$.post('', {
			submit_button: 'Delete',
			filename: filename,
		}).done(function() {
			$('table.list tr td:first-child a').each(function() {
				if ($(this).text() == filename) {
					$(this).parent().parent().remove();
					add_total(-1);

					/* Rmove directory from tree
					 */
					if ($(this).parent().parent().hasClass('directory')) {
						var path = window.location.pathname + filename + '/';
						$('div.structure div.folder a').each(function() {
							if (decodeURIComponent($(this).attr('href')) == path) {
								$(this).parent().parent().remove();
								return false;
							}
						});
					}

					return false;
				}
			});
		}).fail(function(data) {
			alert($(data.responseXML).find('result').text());
		});
	}
}

Dropzone.options.librarian = {
	init: function() {
		this.on("success", function(file) {
			$('table.list tr td:first-child a').each(function() {
				if ($(this).text() == file.name) {
					$(this).parent().parent().remove();
				}
			});

			var name = '<td><a href="' + encodeURIComponent(file.name) + '">' + file.name + '</a></td>';
			var date = '<td>' + now() + '</td>';
			var size = '<td>' + file.size + '</td>';
			$('table.list').append('<tr class="file" onClick="javascript:click_anchor(this)">' + name + date + size + '</tr>');
			add_total(1);
		});

		this.on('complete', function(file) {
			var dropzone = this;
			setTimeout(function() {
				dropzone.removeFile(file)
			}, 5000);
		});
	}
};

$(document).ready(function() {
	var enable_download = $('table.list').attr('download') == 'yes';

	$('table.list tr.directory a').each(function() {
		if ($(this).text() == '..') {
			$(this).parent().parent().addClass('updir');
		} else {
			$(this).parent().parent().addClass('subdir');
		}
	});

	/* Directory menu
	 */
	$.contextMenu({
		selector: 'table.list tr.subdir',
		callback: function(key, options) {
			var file = $(this).find('td:first-child').text();

			switch (key) {
				case 'rename':
					rename_file($(this));
					break;
				case 'delete':
					delete_file(file);
					break;
			}
		},
		items: {
			'rename': {name:'Rename', icon:'fa-edit'},
			'delete': {name:'Delete', icon:'fa-trash'}
		}
	});
	$('table.list tr.updir').contextmenu(function() {
		return false;
	});

	/* Text file menu
	 */
	var items = {
		'edit':     {name:'Edit',     icon:'fa-pencil'},
		'rename':   {name:'Rename',   icon:'fa-edit'},
		'delete':   {name:'Delete',   icon:'fa-trash'}
	}
	if (enable_download) {
		var extra = { 'download': {name:'Download', icon:'fa-download'} }
		items = $.extend({}, extra, items);
	}

	$.contextMenu({
		selector: 'table.list tr.txt',
		callback: function(key, options) {
			var file = $(this).find('td:first-child').text();

			switch (key) {
				case 'download':
					alert(file);
					document.location = encodeURIComponent(file) + '?download';
					break;
				case 'edit':
					$.ajax({
						url: file,
						dataType: "text",
						success:function(data) {
							var form = 
								'<p><input type="hidden" id="filename" value="' + file + '" />' +
								'<textarea id="editfile" class="form-control" onInput="javascript:text_changed=true">' + data + '</textarea></p>';
							text_changed = false;

							var dialog = $(form).windowframe({
								header: file,
								width: 800, 
								buttons: {
									'Save': function() {
										$.post('', {
											content: $('textarea#editfile').val(),
											filename: $('input#filename').val(),
											submit_button: 'SaveFile'
										}).done(function() {
											text_changed = false;
											$(this).close();
										}).fail(function(data) {
											alert($(data.responseXML).find('result').text());
										});
									},
									'Cancel': function() {
										$(this).close();
									}
								},
								close: function() {
									if (text_changed) {
										if (confirm("Discard text changes?") == false) {
											return false;
										}
									}
								}
							});
							dialog.open();
							$('textarea#editfile').focus();
						}
					});
					break;
				case 'rename':
					rename_file($(this));
					break;
				case 'delete':
					delete_file(file);
					break;
			}
		},
		items: items
	});

	/* File menu
	 */
	var items = {
		'rename':   {name:'Rename',   icon:'fa-edit'},
		'delete':   {name:'Delete',   icon:'fa-trash'}
	}
	if (enable_download) {
		var extra = { 'download': {name:'Download', icon:'fa-download'} }
		items = $.extend({}, extra, items);
	}

	$.contextMenu({
		selector: 'table.list tr.file',
		callback: function(key, options) {
			var file = $(this).find('td:first-child').text();

			switch (key) {
				case 'download':
					document.location = encodeURIComponent(file) + '?download';
					break;
				case 'rename':
					rename_file($(this));
					break;
				case 'delete':
					delete_file(file);
					break;
			}
		},
		items: items
	});

	/* Dialogs
	 */
	var newdir_form = '<p><input type="text" id="newdir" class="form-control" /></p>';
	newdir_dialog = $(newdir_form).windowframe({
		header: 'Create new directory',
		buttons: {
			'Create': function() {
				$.post('', {
					filename: $('input#newdir').val(),
					submit_button: 'NewDir'
				}).done(function() {
					$(this).close();
					location.reload();
				}).fail(function(data) {
					alert($(data.responseXML).find('result').text());
				});
			},
			'Cancel': function() {
				$(this).close();
			}
		}
	});

	var newtxtfile_form = '<p><input type="text" id="newtxtfile" value="" class="form-control" /></p>';
	newtxtfile_dialog = $(newtxtfile_form).windowframe({
		header: 'Create new text file',
		buttons: {
			'Create': function() {
				var filename = $('input#newtxtfile').val();
				$.post('', {
					filename: filename,
					submit_button: 'NewTxtFile'
				}).done(function() {
					$(this).close();
					var dir = '<td><a href="' + encodeURIComponent(filename) + '">' + filename + '</a></td>';
					var date = '<td>' + now() + '</td>';
					$('table.list').append('<tr class="file txt" onClick="javascript:click_anchor(this)">' + dir + date + '<td>0</td></tr>');
					add_total(1);

					$('table.list tr.file a').last().each(function() {
						make_draggable($(this));
					});
				}).fail(function(data) {
					alert($(data.responseXML).find('result').text());
				});
			},
			'Cancel': function() {
				$(this).close();
			}
		}
	});

	var rename_form =
		'<p><input type="hidden" id="renamefile_current" value="" />' +
		'<input type="text" id="renamefile_new" value="" class="form-control" /></p>';
	rename_dialog = $(rename_form).windowframe({
		header: 'Rename file or directory',
		buttons: {
			'Rename': function() {
				var filename_current = $('input#renamefile_current').val();
				var filename_new = $('input#renamefile_new').val();

				if (filename_current != filename_new) {
					$.post('', {
						filename_current: filename_current,
						filename_new: filename_new,
						submit_button: 'Rename'
					}).done(function() {
						$(this).close();
						$('table.list tr td:first-child a').each(function() {
							if ($(this).text() == filename_current) {
								$(this).text(filename_new);
								$(this).prop('href', encodeURIComponent(filename_new));

								/* Rename directory in tree
								 */
								if ($(this).parent().parent().hasClass('directory')) {
									var path = window.location.pathname + filename_current + '/';
									$('div.structure div.folder a').each(function() {
										if (decodeURIComponent($(this).attr('href')) == path) {
											$(this).text(filename_new);
											filename_new = encodeURIComponent(filename_new);
											$(this).prop('href', window.location.pathname + filename_new + '/');
											return false;
										}
									});
								}

								return false;
							}
						});
					}).fail(function(data) {
						alert($(data.responseXML).find('result').text());
					});
				} else {
					$(this).close();
				}
			},
			'Cancel': function() {
				$(this).close();
			}
		}
	});

	/* Image viewer
	 */
	var images = 'tr.gif, tr.jpg, tr.jpeg, tr.png, tr.webp';
	$('table.list tr').not(images).find('a').click(function(e) {
		e.stopPropagation();
	});

	$('table.list .menu').click(function(e) {
		e.stopPropagation();
		$(e.target).parent().parent().contextMenu({x:event.pageX - 180, y:event.pageY + 10});
	});

	if (jQuery().magnificPopup) {
  		$(images).attr('onClick', '');
  		$(images).magnificPopup({
			delegate:'a',
			type:'image',
			gallery:{
  				enabled:true
			}
		});
  		$(images).click(function() {
			$(this).find('a').trigger('click');
		});
	}

	/* Moving a file or directory
	 */
	$('table.list tbody td a').each(function() {
		make_draggable($(this));
	});

	$('div.structure div.folder').droppable({
		drop: function(event, ui) {
			var dest = this;
			var source = ui.draggable.text();
			var destination = $(this).find('a').attr('href');

			$.post('', {
				submit_button: 'Move',
				source: source,
				destination: destination
			}).done(function() {
				location.reload();
			}).fail(function(data) {
				alert($(data.responseXML).find('result').text());
				$(dest).removeClass('ui-state-highlight');
			});
		},
		over: function(event, ui) {
			$(this).addClass('ui-state-highlight');
		},
		out: function(event, ui) {
			$(this).removeClass('ui-state-highlight');
		}
	});

	/* Drag & drop
	 */
	$('body').on('dragover', function(event) {
		event.preventDefault();
		event.stopPropagation();
	});

	$('body').on('drop', function(event) {
		event.preventDefault();
		event.stopPropagation();
	});

	/* Logout
	 */
	$('button.logout').click(function() {
		$('body').empty().css('background-color', '#202020');
		window.location = window.location.protocol + '//log:out@' + window.location.hostname;
	});
});
