<?php
	class Command {
		private $view = null;

		public function __construct($view) {
			$this->view = $view;
		}

		private function valid_filename($filename, $url = null) {
			if (strpos($filename, "/") !== false) {
				return false;
			}

			if ($url !== null) {
				if (file_exists(__DIR__."/../../".$url."/".$filename)) {
					return false;
				}
			}

			if (substr($filename, 0, 1) == ".") {
				return false;
			}

			return true;
		}

		private function valid_dirname($dirname) {
			if (strpos($dirname, ".") !== false) {
				return false;
			}

			return true;
		}

		public function handle_post($root, $url) {
			$target = $root.$url."/";
			$result = true;

			switch ($_POST["submit_button"]) {
				case "NewDir":
					if ($this->valid_filename($_POST["filename"], $url) == false) {
						$this->view->add_tag("result", "Invalid filename.");
						$result = false;
					} else if (($result = mkdir($target.$_POST["filename"])) == false) {
						$this->view->add_tag("result", "Can't create directory.");
					}
					break;
				case "NewTxtFile":
					if ($this->valid_filename($_POST["filename"], $url) == false) {
						$this->view->add_tag("result", "Invalid filename.");
						$result = false;
					} else if (file_exists($target.$_POST["filename"])) {
						$this->view->add_tag("result", "File already exists.");
						$result = false;
					} else if (($result = touch($target.$_POST["filename"])) == false) {
						$this->view->add_tag("result", "Can't create directory.");
						$result = false;
					}
					break;
				case "SaveFile":
					if ($this->valid_filename($_POST["filename"]) == false) {
						$this->view->add_tag("result", "Invalid filename.");
						$result = false;
					} else if (($fp = fopen($target.$_POST["filename"], "w")) == false) {
						$this->view->add_tag("result", "Can't write to file.");
						$result = false;
					} else {
						fputs($fp, $_POST["content"]);
						fclose($fp);
					}
					break;
				case "Upload":
					foreach ($_FILES as $file) {
						if ($this->valid_filename($file["name"], $url) == false) {
							$this->view->add_tag("result", "Invalid filename.");
							$result = false;
						} else {
							$destination = $target.$file["name"];
							if (($result = move_uploaded_file($file["tmp_name"], $destination)) == false) {
								$this->view->add_tag("result", "Can't write uploaded file in directory.");
								$result = false;
							}
						}
					}
					break;
				case "Rename":
					if ($this->valid_filename($_POST["filename_current"]) == false) {
						$this->view->add_tag("result", "File or directory not found.");
						$result = false;
					} else if ($this->valid_filename($_POST["filename_new"], $url) == false) {
						$this->view->add_tag("result", "Invalid new name.");
						$result = false;
					} else if (file_exists($target.$_POST["filename_new"])) {
						$this->view->add_tag("result", "New name already exists.");
						$result = false;
					} else {
						$result = rename($target.$_POST["filename_current"], $target.$_POST["filename_new"]);
					}
					break;
				case "Delete":
					if ($this->valid_filename($_POST["filename"]) == false) {
						$this->view->add_tag("result", "Invalid filename.");
						$result = false;
					} else if (is_dir($target.$_POST["filename"]) == false) {
						$result = unlink($target.$_POST["filename"]);
					} else {
						$result = rmdir($target.$_POST["filename"]);
					}
					if ($result == false) {
						$this->view->add_tag("result", "Can't delete file or directory.");
					}
					break;
				case "Move":
					$parts = explode("/", $_POST["source"]);
					$file = array_pop($parts);

					$source = $target.$_POST["source"];
					$destination = $root.urldecode($_POST["destination"]).$file;

					if ($this->valid_filename($_POST["source"], $_POST["destination"]) == false) {
						$this->view->add_tag("result", "Can't do that move.");
						$result = false;
					} else if ($this->valid_dirname($_POST["destination"]) == false) {
						$this->view->add_tag("result", "Invalid destination.");
						$result = false;
					} else if ($source == $destination) {
						$this->view->add_tag("result", "Destination is the same as the source.");
						$result = false;
					} else if (rename($source, $destination) == false) {
						$this->view->add_tag("result", "Can't move file or directory.");
						$result = false;
					}
					break;
				default:
					$this->view->add_tag("result", "Unknown command.");
			}

			if (is_true(DEBUG_MODE)) {
				$output = ob_get_clean();
				if (($fp = fopen("log.txt", "a")) != false) {
					fputs($fp, "--------------------\n");
					fputs($fp, "root: ".$root."\n");
					fputs($fp, "URL: ".$url."\n");
					fputs($fp, "_POST: ".print_r($_POST, true));
					fputs($fp, "_FILES:".print_r($_FILES, true));
					fputs($fp, $output);
					fclose($fp);
				}
			}

			if (($result == false) && ($_SERVER["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest")) {
				header("Result: 500");
			}

			return $result;
		}
	}
?>
