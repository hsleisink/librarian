<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://www.banshee-php.org/
	 *
	 * Licensed under The MIT License
	 */

	/* Exception handler
	 *
	 * INPUT:  error object
	 * OUTPUT: -
	 * ERROR:  -
	 */
	function exception_handler($error) {
		$previous = ob_get_clean();

		header("Content-Type: text/html");
		print "<!DOCTYPE html><html><body>\n";
		print "<h1>Exception</h1>\n";

		if (is_true(DEBUG_MODE)) {
			printf("<p style=\"white-space:pre-wrap\">%s</p>\n", $error->getMessage());
			printf("<p>line %d in %s.</p>\n",  $error->getLine(), $error->getFile());
		} else {
			printf("<p>Contact your website administrator to solve this issue.</p>\n");
			$message = sprintf("%s=> %s\nline %d in %s\n", $previous, $error->getMessage(), $error->getLine(), $error->getFile());
		}

		print "</body></html>\n";
	}

	/* Error handler
	 *
	 * INPUT:  int error number, string error string, string filename, int line number
	 * OUTPUT: -
	 * ERROR:  -
	 */
	function error_handler($errno, $errstr, $errfile, $errline) {
		printf("=> %s\nline %d in %s\n", $errstr, $errline, $errfile);

		return true;
	}

	/* Generate backtrace
	 *
	 * INPUT:  -
	 * OUTPUT: -
	 * ERROR:  -
	 */
	function error_backtrace() {
		$trace = debug_backtrace();
		array_shift($trace);

		$path_offset = strlen(__FILE__) - 33;

		$result = "\nBacktrace:\n";
		foreach ($trace as $step) {
			$result .= sprintf("- %s() at line %d in %s.\n", $step["function"], $step["line"], substr($step["file"], $path_offset));
		}

		return $result;
	}

	/* Error handling settings
	 */
	ini_set("display_errors", 1);
	error_reporting(E_ALL & ~E_NOTICE);
	set_exception_handler("exception_handler");
	set_error_handler("error_handler", E_ALL & ~E_NOTICE);
?>
