<?php
	class Location {
		private $view = null;
		private $locations = null;
		private $keys = null;
		private $count = null;

		public function __construct($view) {
			$this->view = $view;
			$this->locations = config_array(LOCATIONS);
			$this->keys = array_keys($this->locations);
			$this->count = count($this->locations);
		}

		public function get_path() {
			if (isset($_SESSION["location_key"]) == false) {
				$_SESSION["location_key"] = 0;
			}

			if ($this->count == 1) {
				return $this->locations[$this->keys[0]];
			}

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Location") {
					if ($_POST["location"] < $this->count) {
						$_SESSION["location_key"] = (int)$_POST["location"];
					}

					$_SERVER["REQUEST_METHOD"] = "GET";
					$_POST = array();
				}
			}

			return $this->locations[$this->keys[$_SESSION["location_key"]]];
		}

		public function selector() {
			if ($this->count <= 1) {
				return;
			}

			$this->view->open_tag("locations", array("current" => $_SESSION["location_key"]));
			foreach ($this->keys as $key) {
				$this->view->add_tag("location", $key);
			}
			$this->view->close_tag();
		}
	}
?>
