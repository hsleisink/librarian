<?xml version="1.0" ?>

<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
//
//  Path
//
//-->
<xsl:template match="path">
<ul class="path">
<xsl:for-each select="dir">
<li><a href="{@path}"><xsl:value-of select="." /></a></li>
</xsl:for-each>
</ul>
</xsl:template>

<!--
//
//  Structure
//
//-->
<xsl:template match="structure">
<xsl:param name="dir" />
<xsl:variable name="path"><xsl:value-of select="$dir" />/<xsl:value-of select="@name" /></xsl:variable>
<div style="margin-left:{@depth*25}px">
<xsl:if test="structure"><xsl:attribute name="class">leaf</xsl:attribute></xsl:if>
<div class="folder">
<xsl:if test="structure">
	<xsl:if test="@status='open'">
		<img src="/librarian/images/arrow1.png" class="node" onClick="javascript:toggle_trunk(this)" />
	</xsl:if>
	<xsl:if test="@status='closed'">
		<img src="/librarian/images/arrow0.png" class="node" onClick="javascript:toggle_trunk(this)" />
	</xsl:if>
</xsl:if>
<xsl:if test="not(structure)">
	<img src="/librarian/images/empty.png" class="node" />
</xsl:if>
<img src="/librarian/images/directory.png" class="node" /><a href="{$path}/" class="{@status}_{@type}"><xsl:value-of select="@name" /></a>
</div>
</div>
<xsl:if test="structure">
	<div class="trunk {@status}">
	<xsl:apply-templates select="structure"><xsl:with-param name="dir" select="$path" /></xsl:apply-templates>
	</div>
</xsl:if>
</xsl:template>

<!--
//
//  List
//
//-->
<xsl:template match="list">
<table class="table table-striped table-hover list" download="{@download}">
<thead>
<tr><th>Name</th><th>Modified</th><th>Size</th><th></th></tr>
</thead>
<tbody>
<xsl:for-each select="directory">
<tr class="directory" onClick="javascript:click_anchor(this)">
<td><a href="{link}/"><xsl:value-of select="name" /></a></td>
<td><xsl:value-of select="modified" /></td>
<td></td>
<td><xsl:if test="name!='..'"><i class="fa fa-chevron-down btn btn-primary btn-xs menu"></i></xsl:if></td>
</tr>
</xsl:for-each>
<xsl:for-each select="file">
<tr class="file {ext}" onClick="javascript:click_anchor(this)">
<td><a href="{link}"><xsl:value-of select="name" /></a></td>
<td><xsl:value-of select="modified" /></td>
<td><xsl:value-of select="size" /></td>
<td><i class="fa fa-chevron-down btn btn-primary btn-xs menu"></i></td>
</tr>
</xsl:for-each>
</tbody>
<tfoot>
<tr><td></td><td></td><td>Total: <xsl:value-of select="count(directory) + count(file)"/></td><td></td></tr>
</tfoot>
</table>
</xsl:template>

<!--
//
//  Dropzone
//
//-->
<xsl:template name="dropzone">
<form action="{@url}/" class="dropzone" id="librarian">
<input type="hidden" name="submit_button" value="Upload" />
</form>
</xsl:template>

<!--
//
//  Directory
//
//-->
<xsl:template match="directory">
<div class="row">
<div class="col-lg-3 col-md-4">
<div class="structure">
<div class="folder"><img src="/librarian/images/directory.png" class="node" /><a href="/"><xsl:value-of select="/output/locations/location[../@current=(position()-1)]" /></a></div>
<xsl:apply-templates select="structure" />
<div class="btn-group">
<button class="btn btn-default logout">Logout</button>
</div>
</div>
</div>
<div class="col-lg-9 col-md-8">
<xsl:apply-templates select="path" />
<h2><xsl:value-of select="path/dir[position()=count(../dir)]" /></h2>
<div class="buttons">
<input type="button" value="New directory" class="btn btn-primary btn-xs" onClick="javascript:new_directory()" />
<input type="button" value="New text file" class="btn btn-primary btn-xs" onClick="javascript:new_text_file()" />
</div>
<xsl:apply-templates select="list" />
<xsl:call-template name="dropzone" />
</div>
</div>
</xsl:template>

</xsl:stylesheet>
