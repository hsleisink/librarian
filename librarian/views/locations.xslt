<?xml version="1.0" ?>

<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
//
//  Locations
//
//-->
<xsl:template match="locations">
<form action="/" method="post" onChange="javascript:submit()" class="locations">
<select name="location" class="form-control">
<xsl:for-each select="location">
<option value="{position()-1}"><xsl:if test="position()-1=../@current"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
<input type="hidden" name="submit_button" value="Location" />
</form>
</xsl:template>

</xsl:stylesheet>
