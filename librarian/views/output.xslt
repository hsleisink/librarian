<?xml version="1.0" ?>

<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="locations.xslt" />
<xsl:import href="directory.xslt" />
<xsl:import href="error.xslt" />

<xsl:output method="html" doctype-system="about:legacy-compat" />

<xsl:template match="redirect">
<p>Click <a href="{.}">here</a> to continue<xsl:if test="@delay"> or wait <xsl:value-of select="@delay" /> seconds to be redirected</xsl:if>.</p>
<xsl:if test="@delay">
<script type="text/javascript">
	setTimeout(function() {
		document.location = '<xsl:value-of select="." />';
	}, <xsl:value-of select="@delay" />000);
</script>
</xsl:if>
</xsl:template>

<xsl:template match="/output">
<html lang="en">

<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="generator" content="File" />
<meta http-equiv="refresh" content="{@timeout}; url=/" />
<link rel="apple-touch-icon" href="/librarian/images/library.png" />
<link rel="icon" href="/librarian/images/library.png" />
<link rel="shortcut icon" href="/librarian/images/library.png" />
<title>Librarian</title>
<xsl:for-each select="styles/style">
<link rel="stylesheet" type="text/css" href="/librarian/css/{.}" />
</xsl:for-each>
<xsl:for-each select="javascripts/javascript">
<script type="text/javascript" src="/librarian/js/{.}" /><xsl:text>
</xsl:text></xsl:for-each>
</head>

<body>
<div class="header">
	<div class="container">
		<h1>Librarian</h1>
		<img src="/librarian/images/library.png" class="logo" />
	</div>
</div>

<div class="content">
	<div class="container">
		<xsl:apply-templates select="locations" />
		<xsl:apply-templates select="directory" />
		<xsl:apply-templates select="error" />
		<xsl:apply-templates select="redirect" />
	</div>
</div>
</body>

</html>
</xsl:template>

</xsl:stylesheet>
