<?xml version="1.0" ?>

<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="error">
<h1>Error</h1>
<p><xsl:value-of select="." /></p>
</xsl:template>

</xsl:stylesheet>
