Librarian
=========

Librarian is a web based file manager, written in PHP and jQuery and offers
the following features:

- Manage multiple directories.
- Upload files via drag & drop.
- Create directories and text files.
- View images in browsable viewer.
- Edit text files.
- Delete files and empty directories.
- Move files and directories via drag & drop.
- Support for mobile devices.
