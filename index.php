<?php
	ob_start();
	session_start();

	require "librarian/libraries/error.php";
	require "librarian/libraries/general.php";
	require "librarian/libraries/location.php";
	require "librarian/libraries/xml.php";

	$view = new XML();

	/* Determine target
	 */
	list($url) = explode("?", $_SERVER["REQUEST_URI"], 2);
	$url = urldecode(rtrim($url, "/"));

	$location = new Location($view);
	$root = $location->get_path();
	if (substr($root, 0, 1) != "/") {
		$root = __DIR__."/".$root;
	}
	$target = $root.$url;

	/* Handle POST requests
	 */
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		require "librarian/libraries/commands.php";
		$command = new command($view);

		$view->open_tag("output");
		$result = $command->handle_post($root, $url);
		$view->close_tag();

		if ($result == false) {
			header("Status: 403");
		}

		ob_end_clean();
		header("Content-Type: text/xml");
		print $view->document;

		exit;
	}

	require "librarian/libraries/directory.php";

	/* Target is file
	 */
	if (is_file($target)) {
		ob_end_clean();
		if (is_true(USE_SENDFILE)) {
			header("X-Sendfile: ".$target);
		} else {
			if (($mimetype = get_mimetype($target)) != false) {
				header("Content-Type: ".$mimetype);
			}
			if (isset($_GET["download"])) {
				header("Content-Disposition: attachment");
			}
			readfile($target);
		}
		exit;
	}

	list($uri) = explode("?", $_SERVER["REQUEST_URI"], 2);
	if (substr($uri, -1) != "/") {
		header("Status: 301");
		header("Location: ".$uri."/");
		exit;
	}

	$session_timeout = ini_get("session.gc_maxlifetime");

	$view->open_tag("output", array("timeout" => $session_timeout));

	/* Stylesheets
	 */
	$view->open_tag("styles");
	$view->add_tag("style", "bootstrap.css");
	$view->add_tag("style", "bootstrap-theme.css");
	$view->add_tag("style", "jquery.ui.css");
	$view->add_tag("style", "dropzone.css");
	$view->add_tag("style", "librarian.css");
	if (is_true(HIDE_LIST_DIRECTORY)) {
		$view->add_tag("style", "hide_list_dir.css");
	}
	$view->add_tag("style", "jquery.contextMenu.css");
	$view->add_tag("style", "font-awesome.min.css");
	if (is_true(IMAGE_VIEWER)) {
		$view->add_tag("style", "magnific-popup.css");
	}
	$view->close_tag();

	/* Javascripts
	 */
	$view->open_tag("javascripts");
	$view->add_tag("javascript", "jquery.js");
	$view->add_tag("javascript", "bootstrap.js");
	$view->add_tag("javascript", "dropzone.js");
	$view->add_tag("javascript", "librarian.js");
	$view->add_tag("javascript", "jquery.contextMenu.js");
	$view->add_tag("javascript", "jquery.ui.js");
	$view->add_tag("javascript", "jquery.windowframe.js");
	if (is_true(IMAGE_VIEWER)) {
		$view->add_tag("javascript", "jquery.magnific-popup.js");
	}
	$view->close_tag();

	/* Location selector
	 */
	$location->selector();

	/* Show directory index
	 */
	$directory = new DirectoryIndex($view);

	if ($directory->list($root, $url) == false) {
		$view->add_tag("error", "Can't find or access the specified directory.");
		$view->add_tag("redirect", "/", array("delay" => 3));
	}

	$view->close_tag();

	/* Generate output
	 */
	if (($errors = ob_get_clean()) != "") {
		header("Content-Type: text/plain");
		print $errors;
	} else {
		if (($_GET["output"] ?? null) == "xml") {
			header("Content-Type: text/xml");
			print $view->document;
		} else if (($html = $view->transform("output.xslt")) == false) {
			print "Transform error";
		} else {
			print $html;
		}
	}
?>
